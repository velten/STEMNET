To install from within R, you can use the `install_git` function from the `devtools` package.

```
install.packages("devtools")
devtools::install_git("https://git.embl.de/velten/STEMNET/", build_vignettes=TRUE)
```

To understand how STEMNET is used, refer to the package vignette.  Our basic analysis strategy is easily summarized: We first use hierarchical clustering to identify the most mature cell populations; we then use these populations as a training set to classify priming in the less mature populations.

UPDATE 2018/04/22: Some users had problem with this command, a way to install that works for everyone is to download the tar.gz archive, unpack to a directory of your choice (e.g. /path/to) and inside R run
devtools::install_local("/path/to/stemnet_dir", build_vignettes=T) 
